$(document).ready(function () {
    $("#table_programmers").DataTable({
        lengthMenu: [50, 100],
        ajax: {
            "url": "/programmers.json",
            "dataSrc": (json) => {
                return json.map((programmer) => {
                    programmer.date_of_birthday = programmer.date_of_birthday.replace(/-/g, ".");
                    programmer.phone_numbers = programmer.phone_numbers.filter(String).join(", ");
                    programmer.details = `<a href="/programmers/${programmer.id}">Show</a>`;
                    programmer.languages = programmer.languages.map((language) => language.name).join(', ');
                    programmer.avatar = programmer.avatar.url ? `<image src="${programmer.avatar.url}" alt="avatar" width="50" class="rounded mx-auto d-block"> </image>` : "";
                    return programmer;
                })
            }
        },
        columns: [{
                "data": "id",
                "searchable": false
            },
            {
                "data": "first_name"
            },
            {
                "data": "last_name"
            },
            {
                "data": "email",
                "searchable": false
            },
            {
                "data": "date_of_birthday",
                "searchable": false
            },
            {
                "data": "phone_numbers",
                "searchable": false
            },
            {
                "data": "languages",
                "searchable": false
            },
            {
                "data": "avatar",
                "searchable": false
            },
            {
                "data": "details",
                "searchable": false
            },
        ],
    });
});


$(document).ready(function () {
    $("#table_languages").DataTable({
        lengthMenu: [50, 100],
        ajax: {
            "url": "/languages.json",
            "dataSrc": (json) => {
                return json.map((data) => {
                    data.details = `<a href="/languages/${data.id}">Show</a>`;
                    return data;
                })
            }
        },
        columns: [{
                "data": "id",
                "searchable": false
            },
            {
                "data": "name"
            },
            {
                "data": "slug",
                "searchable": false
            },
            {
                "data": "active",
                "searchable": false
            },
            {
                "data": "details",
                "searchable": false
            }
        ],
    });
});