class Admin::AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :check_admin

  protected

  def check_admin
    redirect_to home_path, alert: "You do not have the right." unless current_user.admin?
  end
end
