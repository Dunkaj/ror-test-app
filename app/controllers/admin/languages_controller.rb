class Admin::LanguagesController < Admin::AdminController
  before_action :set_language, only: %i[edit destroy update]

  def edit
  end

  def new
    @language = Language.new
  end

  def create
    @language = Language.new(language_params)
    if @language.save
      redirect_to @language
    else
      render "new"
    end
  end

  def destroy
    @language.destroy
    redirect_to languages_path
  end

  def update
    if (@language.update(language_params))
      redirect_to @language
    else
      render "edit"
    end
  end

  private

  def language_params
    params.require(:language).permit(:name, :active)
  end

  def set_language
    @language = Language.friendly.find(params[:id])
    redirect_to home_path if @language.nil?
  end
end
