class Admin::ProgrammersController < Admin::AdminController
  before_action :set_programmer, only: %i[edit destroy update]
  before_action :set_languages, only: %i[new edit create update]
  before_action :set_programmerLanguages, only: %i[edit update]

  def new
    @programmer = Programmer.new
  end

  def create
    @programmer = Programmer.new(programmer_params)
    @programmer.language_ids = params[:programmer][:languages]
    if @programmer.save
      redirect_to @programmer
    else
      render "new"
    end
  end

  def edit
  end

  def update
    if (@programmer.update(programmer_params) && @programmer.language_ids = params[:programmer][:languages])
      redirect_to @programmer
    else
      render "edit"
    end
  end

  def destroy
    @programmer.destroy
    redirect_to programmers_path
  end

  private

  def programmer_params
    params.require(:programmer).permit(:first_name, :last_name, :email, :avatar, :date_of_birthday, phone_numbers: [])
  end

  def set_programmer
    @programmer = Programmer.find(params[:id])
    redirect_to home_path if @programmer.nil?
  end

  def set_languages
    @languages = Language.all
    redirect_to home_path if @languages.nil?
  end

  def set_programmerLanguages
    @programmerLanguages = @programmer.languages.map { |language| language.id }
    redirect_to home_path if @programmerLanguages.nil?
  end
end
