class LanguagesController < ApplicationController
  def index
    @languages = Language.all
    respond_to do |format|
      format.html
      format.json { render json: @languages }
    end
  end

  def show
    @language = Language.friendly.find(params[:id])
  end
end
