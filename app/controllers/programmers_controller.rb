class ProgrammersController < ApplicationController
  def index
    @programmers = Programmer.includes(:languages).all
    @programmersAndLanguages = []
    @programmers.length.times do |i|
      @programmersAndLanguages[i] = @programmers[i].as_json(methods: :languages)
    end
    respond_to do |format|
      format.html
      format.json { render json: @programmersAndLanguages }
    end
  end

  def show
    @programmer = Programmer.find(params[:id])
    @languages = @programmer.languages.map { |language| language.name }.join(", ")
  end
end
