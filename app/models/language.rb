class Language < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_and_belongs_to_many :programmers
  acts_as_paranoid

  validates :name, length: {in: 1..15}, presence: true, uniqueness: true
end
