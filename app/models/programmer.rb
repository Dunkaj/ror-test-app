class Programmer < ActiveRecord::Base
  serialize :phone_numbers, Array
  has_and_belongs_to_many :languages
  acts_as_paranoid
  mount_uploader :avatar, AvatarUploader

  validates :first_name, :last_name, length: {in: 2..20}
  # validates :phone_numbers, length: {is: 13}
  validates :email, format: {
                      with: URI::MailTo::EMAIL_REGEXP,
                      message: "Only valid emails allowed",
                    }, uniqueness: true
  validates :first_name, :last_name, :email, :avatar, :date_of_birthday, :phone_numbers, presence: true
end
