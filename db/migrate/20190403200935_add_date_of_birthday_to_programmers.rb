class AddDateOfBirthdayToProgrammers < ActiveRecord::Migration
  def change
    add_column :programmers, :date_of_birthday, :date
  end
end
