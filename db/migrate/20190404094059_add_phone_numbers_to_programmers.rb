class AddPhoneNumbersToProgrammers < ActiveRecord::Migration
  def change
    add_column :programmers, :phone_numbers, :string
  end
end
