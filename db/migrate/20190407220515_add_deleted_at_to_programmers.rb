class AddDeletedAtToProgrammers < ActiveRecord::Migration
  def change
    add_column :programmers, :deleted_at, :datetime
    add_index :programmers, :deleted_at
  end
end
