class AddAvatarToProgrammers < ActiveRecord::Migration
  def change
    add_column :programmers, :avatar, :string
  end
end
